package io.github.epocopa.simplenews

import androidx.lifecycle.*
import me.toptas.rssconverter.RssItem

private const val TAG = "ArticleListViewModel"

class ArticleListViewModel : ViewModel() {
    val repository: Repository = Repository.get()
    var rssItemLiveData: LiveData<List<RssItem>>
    private val originListLiveData: LiveData<List<Origin>>
    private var updateTrigger = MediatorLiveData<List<Origin>>()
    private val feedfetcher = FeedFetcher()

    init {
        originListLiveData = repository.getOrigins()
        updateTrigger.addSource(originListLiveData, updateTrigger::setValue)
        rssItemLiveData = Transformations.switchMap(updateTrigger) { origins ->
            feedfetcher.fetchAll(origins)
        }
    }

    fun updateFeeds() {
        updateTrigger.value = originListLiveData.value
    }
}
