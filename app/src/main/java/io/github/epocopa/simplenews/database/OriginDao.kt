package io.github.epocopa.simplenews.database

import androidx.lifecycle.LiveData
import androidx.room.*
import io.github.epocopa.simplenews.Origin
import java.net.URL

@Dao
interface OriginDao {

    @Query("SELECT * from origin")
    fun getOrigins(): LiveData<List<Origin>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addOrigin(origin: Origin)

    @Update
    fun updateOrigin(origin: Origin)

    @Delete
    fun deleteOrigin(origin: Origin)

    // TODO remove, for debugging purposes
    @Query("DELETE FROM origin WHERE url=(:url)")
    fun deleteOrigin(url: URL)

    // TODO remove, for debugging purposes
    @Query("DELETE FROM origin")
    fun clearOrigins()
}
