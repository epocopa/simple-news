package io.github.epocopa.simplenews

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import me.toptas.rssconverter.RssItem


class ArticleListFragment : Fragment() {
    private var listener: Callbacks? = null
    private lateinit var articleRecyclerView: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var emptyListTextView: TextView

    private val articleListViewModel: ArticleListViewModel by lazy {
        ViewModelProvider(this).get(ArticleListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_article_list, container, false)
        articleRecyclerView = view.findViewById(R.id.article_recycler_view)
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout)
        swipeRefreshLayout.setOnRefreshListener {
            val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
            val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
            if (isConnected) {
                (articleRecyclerView.adapter as ArticleListRecyclerViewAdapter).list.clear()
                articleListViewModel.updateFeeds()
            } else {
                Toast.makeText(context, getString(R.string.no_internet), Toast.LENGTH_LONG).show()
            }
            swipeRefreshLayout.isRefreshing = false
        }
        emptyListTextView = view.findViewById(R.id.empty_view)

        articleRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = ArticleListRecyclerViewAdapter(listener)
        }

        if ((articleRecyclerView.adapter as ArticleListRecyclerViewAdapter).list.size() == 0) {
            articleRecyclerView.visibility = View.GONE
            emptyListTextView.visibility = View.VISIBLE
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        articleListViewModel.rssItemLiveData.observe(
            viewLifecycleOwner,
            Observer { rssItems ->
                if (rssItems.isNullOrEmpty()) {
                    articleRecyclerView.visibility = View.GONE
                    emptyListTextView.visibility = View.VISIBLE
                } else {
                    articleRecyclerView.visibility = View.VISIBLE
                    emptyListTextView.visibility = View.GONE
                    (articleRecyclerView.adapter as ArticleListRecyclerViewAdapter).list.addAll(rssItems)
                    articleRecyclerView.adapter?.notifyDataSetChanged()
                }
            }
        )
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Callbacks) {
            listener = context
        } else {
            throw RuntimeException("$context must implement Callbacks")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_article_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.scroll_to_top -> {
                articleRecyclerView.smoothScrollToPosition(0)
                true
            }
            R.id.logout -> {
                AuthUI.getInstance()
                    .signOut(requireContext())
                    .addOnCompleteListener {
                        listener?.onSignOut()
                    }
                articleListViewModel.repository.clearOrigins()
                articleListViewModel.repository.clearArticles()
                (articleRecyclerView.adapter as ArticleListRecyclerViewAdapter).list.clear()
                if (FirebaseAuth.getInstance().currentUser?.isAnonymous!!) {
                    FirebaseAuth.getInstance().currentUser?.delete()
                }
                true
            }
            R.id.favorites -> {
                listener?.showFavs()
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    interface Callbacks {
        fun onFeedClick(article: RssItem)
        fun onSignOut()
        fun showFavs()
        fun addFav(article: Article)
    }
}
