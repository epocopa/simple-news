package io.github.epocopa.simplenews

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.net.URL

@Entity
data class Origin (@PrimaryKey val url: URL)
