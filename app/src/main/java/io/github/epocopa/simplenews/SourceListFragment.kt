package io.github.epocopa.simplenews

import androidx.appcompat.app.AlertDialog
import android.os.Bundle
import android.view.*
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.net.MalformedURLException
import java.net.URL


class SourceListFragment : Fragment(), OnDeleteItem {
    private lateinit var sourceRecyclerView: RecyclerView
    private lateinit var emptyListTextView: TextView
    private val sourceListViewModel: SourceListViewModel by lazy {
        ViewModelProvider(this).get(SourceListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_sources_list, container, false)
        emptyListTextView = view.findViewById(R.id.empty_view)
        sourceRecyclerView = view.findViewById(R.id.source_recycler_view)
        sourceRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = SourceListRecyclerViewAdapter(mutableListOf(), this@SourceListFragment)
        }
        if ((sourceRecyclerView.adapter as SourceListRecyclerViewAdapter).list.isEmpty()) {
            sourceRecyclerView.visibility = View.GONE
            emptyListTextView.visibility = View.VISIBLE
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sourceListViewModel.sourcesLiveData.observe(
            viewLifecycleOwner,
            Observer { items ->
                if (items.isNullOrEmpty()) {
                    sourceRecyclerView.visibility = View.GONE
                    emptyListTextView.visibility = View.VISIBLE
                } else {
                    sourceRecyclerView.visibility = View.VISIBLE
                    emptyListTextView.visibility = View.GONE
                }
                (sourceRecyclerView.adapter as SourceListRecyclerViewAdapter).list.clear()
                (sourceRecyclerView.adapter as SourceListRecyclerViewAdapter).list.addAll(items)
                sourceRecyclerView.adapter?.notifyDataSetChanged()
            }
        )
        val onDeleteCallback = ItemTouchHelper(
            OnDeleteCallback(
                (sourceRecyclerView.adapter as DeleteItem), requireContext()
            )
        )
        onDeleteCallback.attachToRecyclerView(sourceRecyclerView)
    }

    override fun onDeleteItem(url: Any) {
        sourceListViewModel.repository.deleteOrigin(url as URL)
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.fragment_source_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.add_source -> {
                val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
                builder.setTitle(getString(R.string.add_source))
                val viewInflated: View = LayoutInflater.from(context)
                    .inflate(R.layout.input_source, view as ViewGroup?, false)
                val input = viewInflated.findViewById(R.id.input) as EditText
                builder.setView(viewInflated)

                builder.setPositiveButton(
                    android.R.string.ok
                ) { dialog, _ ->
                    dialog.dismiss()
                    val text = input.text.toString()
                    try {
                        val origin = Origin(URL(text))
                        sourceListViewModel.repository.addOrigin(origin)
                    } catch (e: MalformedURLException) {
                        Toast.makeText(
                            activity, getString(R.string.invalid_url),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
                builder.setNegativeButton(
                    android.R.string.cancel
                ) { dialog, _ -> dialog.cancel() }
                builder.show()
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}
