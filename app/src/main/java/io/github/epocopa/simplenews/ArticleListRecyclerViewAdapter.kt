package io.github.epocopa.simplenews


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import io.github.epocopa.simplenews.ArticleListFragment.Callbacks
import me.toptas.rssconverter.RssItem
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ArticleListRecyclerViewAdapter(
    private val listener: Callbacks?
) : RecyclerView.Adapter<ArticleListRecyclerViewAdapter.ViewHolder>() {

    val list: SortedList<RssItem>
    private val mOnClickListener: View.OnClickListener
    private val mOnLongClickListener: View.OnLongClickListener
    private val formatter: DateFormat = SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z", Locale.US)

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as RssItem
            listener?.onFeedClick(item)
        }

        mOnLongClickListener = View.OnLongClickListener { v ->
            val item = (v.tag as RssItem)
            val article = Article(item.title!!, item.link!!, Date())
            listener?.addFav(article)
            Toast.makeText(listener as Context, "Article added to favorites", Toast.LENGTH_SHORT).show()
            true
        }

        list = SortedList(RssItem::class.java, object: SortedListAdapterCallback<RssItem>(this) {
            override fun compare(o1: RssItem?, o2: RssItem?): Int {
                val date1: Date?
                val date2: Date?
                try {
                    date1 = formatter.parse(o1?.publishDate!!)
                    date2 = formatter.parse(o2?.publishDate!!)
                }
                catch (e: java.text.ParseException) {
                    return 1
                }
                catch (e: KotlinNullPointerException){
                    return 1
                }
                return date2!!.compareTo(date1)
            }

            override fun areContentsTheSame(oldItem: RssItem?, newItem: RssItem?): Boolean {
                return oldItem == newItem
            }

            override fun areItemsTheSame(item1: RssItem?, item2: RssItem?): Boolean {
                return item1 === item2
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_article, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.apply {
            title.text = item.title
            if (item.description.isNullOrEmpty() || item.description!!.trimStart()[0] == '<')
                description.text = ""
            else
                description.text = item.description
        }

        with(holder.view) {
            tag = item
            setOnClickListener(mOnClickListener)
            setOnLongClickListener(mOnLongClickListener)
        }
    }

    override fun getItemCount(): Int = list.size()

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.findViewById(R.id.article_title)
        val description: TextView = view.findViewById(R.id.article_description)
    }
}
