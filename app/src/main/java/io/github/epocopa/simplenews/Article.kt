package io.github.epocopa.simplenews

import androidx.room.Entity
import java.util.*

@Entity(primaryKeys = ["title", "origin"])
data class Article(val title:String, val origin:String, val date: Date)