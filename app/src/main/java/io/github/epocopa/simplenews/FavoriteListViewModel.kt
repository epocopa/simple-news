package io.github.epocopa.simplenews

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

class FavoriteListViewModel : ViewModel() {
    val repository: Repository = Repository.get()
    var articleLiveData: LiveData<List<Article>>

    init {
        articleLiveData = repository.getArticles()
    }
}