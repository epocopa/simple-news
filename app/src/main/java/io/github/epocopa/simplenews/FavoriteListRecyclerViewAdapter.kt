package io.github.epocopa.simplenews

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import androidx.recyclerview.widget.SortedListAdapterCallback
import io.github.epocopa.simplenews.ArticleListFragment.*
import me.toptas.rssconverter.RssItem
import java.text.SimpleDateFormat

class FavoriteListRecyclerViewAdapter(
    private val clicklistener: Callbacks?,
    private val deletelistener: OnDeleteItem?
) : RecyclerView.Adapter<FavoriteListRecyclerViewAdapter.ViewHolder>(), DeleteItem {

    val list: SortedList<Article>
    private val mOnClickListener: View.OnClickListener
    private val dateFormatter = SimpleDateFormat("MM/dd/yyyy HH:mm")

    init {
        mOnClickListener = View.OnClickListener { v ->
            val article = (v.tag as Article)
            val item = RssItem()
            item.link = article.origin
            clicklistener?.onFeedClick(item)
        }

        list = SortedList(Article::class.java, object: SortedListAdapterCallback<Article>(this) {
            override fun compare(o1: Article?, o2: Article?): Int {
                if (o1 == null)
                    return 1
                else if (o2 == null)
                    return -1
                return o1.date.compareTo(o2.date)
            }

            override fun areContentsTheSame(oldItem: Article?, newItem: Article?): Boolean {
                return oldItem == newItem
            }

            override fun areItemsTheSame(item1: Article?, item2: Article?): Boolean {
                return item1 === item2
            }
        })
    }

    override fun deleteItem(position: Int) {
        deletelistener?.onDeleteItem(list[position])
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FavoriteListRecyclerViewAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_favorite, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size()
    }

    override fun onBindViewHolder(holder: FavoriteListRecyclerViewAdapter.ViewHolder, position: Int) {
        val item = list[position]
        holder.apply {
            title.text = item.title
            date.text = dateFormatter.format(item.date)
        }

        with(holder.view) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.findViewById(R.id.fav_title)
        val date: TextView = view.findViewById(R.id.fav_date)
    }
}