package io.github.epocopa.simplenews

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.github.epocopa.simplenews.ArticleListFragment.Callbacks

class FavoriteListFragment : Fragment(), OnDeleteItem {
    private var listener: Callbacks? = null
    private lateinit var favRecyclerView: RecyclerView
    private lateinit var emptyListTextView: TextView

    private val favoriteListViewModel: FavoriteListViewModel by lazy {
        ViewModelProvider(this).get(FavoriteListViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_favorite_list, container, false)
        favRecyclerView = view.findViewById(R.id.fav_recycler_view)
        favRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = FavoriteListRecyclerViewAdapter(listener, this@FavoriteListFragment)
        }
        emptyListTextView = view.findViewById(R.id.empty_favorites)
        if ((favRecyclerView.adapter as FavoriteListRecyclerViewAdapter).list.size() == 0) {
            favRecyclerView.visibility = View.GONE
            emptyListTextView.visibility = View.VISIBLE
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        favoriteListViewModel.articleLiveData.observe(
            viewLifecycleOwner,
            Observer { favorites ->
                if (favorites.isNullOrEmpty()) {
                    favRecyclerView.visibility = View.GONE
                    emptyListTextView.visibility = View.VISIBLE
                } else {
                    favRecyclerView.visibility = View.VISIBLE
                    emptyListTextView.visibility = View.GONE
                }
                (favRecyclerView.adapter as FavoriteListRecyclerViewAdapter).list.clear()
                (favRecyclerView.adapter as FavoriteListRecyclerViewAdapter).list.addAll(favorites)
                favRecyclerView.adapter?.notifyDataSetChanged()
            }
        )
        val onDeleteCallback = ItemTouchHelper(
            OnDeleteCallback(
                (favRecyclerView.adapter as DeleteItem), requireContext()
            )
        )
        onDeleteCallback.attachToRecyclerView(favRecyclerView)
    }

    override fun onDeleteItem(article: Any) {
        favoriteListViewModel.repository.deleteArticle(article as Article)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Callbacks) {
            listener = context
        } else {
            throw RuntimeException("$context must implement Callbacks")
        }
    }

    fun addFav(article: Article) {
        favoriteListViewModel.repository.addArticle(article)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

}