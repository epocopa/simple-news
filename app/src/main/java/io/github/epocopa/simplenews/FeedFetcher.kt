package io.github.epocopa.simplenews

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import me.toptas.rssconverter.RssConverterFactory
import me.toptas.rssconverter.RssFeed
import me.toptas.rssconverter.RssItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.net.URL


private const val TAG = "FeedFetcher"

class FeedFetcher {

    private val rssService: RssService

    init {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("http://localhost/")
            .addConverterFactory(RssConverterFactory.create())
            .build()

        rssService = retrofit.create(RssService::class.java)
    }

    private fun fetchRss(url: URL): LiveData<List<RssItem>> {
        val responseLiveData: MutableLiveData<List<RssItem>> = MutableLiveData()
        rssService.fetchRss(url.toString()).enqueue(object: Callback<RssFeed> {
            override fun onResponse(call: Call<RssFeed>, response: Response<RssFeed>) {
                if (response.isSuccessful) {
                    responseLiveData.value = response.body()?.items
                } else {
                    Log.e(TAG, "Failed to fetchRss RSS feed! Response code: " + response.code())
                }
            }
            override fun onFailure(call: Call<RssFeed>, t: Throwable) {
                Log.e(TAG, "Failed to fetchRss RSS feed!", t)
            }
        })
        return responseLiveData

    }

    fun fetchAll(origins: List<Origin>): LiveData<List<RssItem>> {
        val result = MediatorLiveData<List<RssItem>>()
        for (origin in origins) {
            result.addSource(fetchRss(origin.url), result::setValue)
        }
        return result
    }
}
