package io.github.epocopa.simplenews

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import io.github.epocopa.simplenews.database.Database
import java.lang.IllegalStateException
import java.net.URL
import java.util.concurrent.Executors

private const val TAG = "Repository"
private const val DATABASE_NAME = "origin-database"
private const val SOURCES  = "sources"
private const val FAVS  = "favorites"

class Repository private constructor(context: Context) {

    private val database: Database = Room.databaseBuilder(
        context.applicationContext,
        Database::class.java,
        DATABASE_NAME
    ).build()

    private val originDao = database.originDao()
    private val articleDao = database.articleDao()
    private val executor = Executors.newSingleThreadExecutor()
    private val db = FirebaseFirestore.getInstance()
    private var auth: FirebaseAuth = FirebaseAuth.getInstance()

    fun getOrigins(): LiveData<List<Origin>> = originDao.getOrigins()

    fun addOrigin(origin: Origin) {
        executor.execute {
            originDao.addOrigin(origin)
        }

        if (!FirebaseAuth.getInstance().currentUser?.isAnonymous!!) {
            db.collection(SOURCES).document(auth.currentUser?.email!!)
                .update(SOURCES, FieldValue.arrayUnion(origin.url.toString()))
        }
    }

    fun addOriginOffline(string: String) {
        executor.execute {
            originDao.addOrigin(Origin(URL(string)))
        }
    }

    fun updateOrigin(origin: Origin) {
        executor.execute {
            originDao.updateOrigin(origin)
        }
    }

    fun deleteOrigin(origin: Origin) {
        executor.execute {
            originDao.deleteOrigin(origin)
        }
    }

    fun deleteOrigin(url: URL) {
        executor.execute {
            originDao.deleteOrigin(url)
        }

        if (!FirebaseAuth.getInstance().currentUser?.isAnonymous!!) {
            db.collection(SOURCES).document(auth.currentUser?.email!!)
                .update(SOURCES, FieldValue.arrayRemove(url.toString()))
        }
    }

    fun clearOrigins() {
        executor.execute {
            originDao.clearOrigins()
        }
    }

    fun getArticles(): LiveData<List<Article>> = articleDao.getArticles()

    fun addArticle(article: Article) {
        executor.execute {
            articleDao.addArticle(article)
        }

        if (!FirebaseAuth.getInstance().currentUser?.isAnonymous!!) {
            db.collection(FAVS).document(auth.currentUser?.email!!)
                .update(FAVS, FieldValue.arrayUnion(article))
        }
    }

    fun addArticleOffline(article: Article) {
        executor.execute {
            articleDao.addArticle(article)
        }
    }


    fun deleteArticle(article: Article) {
        executor.execute {
            articleDao.deleteArticle(article)
        }

        if (!FirebaseAuth.getInstance().currentUser?.isAnonymous!!) {
            db.collection(FAVS).document(auth.currentUser?.email!!)
                .update(FAVS, FieldValue.arrayRemove(article))
        }
    }

    fun clearArticles() {
        executor.execute {
            articleDao.clearArticles()
        }
    }


    companion object {
        private var INSTANCE: Repository? = null

        fun initialize(context: Context) {
            if (INSTANCE == null) {
                INSTANCE = Repository(context)
            }
        }

        fun get(): Repository {
            return INSTANCE ?: throw IllegalStateException("Repository not initialized")
        }
    }
}
