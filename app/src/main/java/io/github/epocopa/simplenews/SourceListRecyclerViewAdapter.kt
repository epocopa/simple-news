package io.github.epocopa.simplenews

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

interface OnDeleteItem {
    fun onDeleteItem(any: Any)
}

interface DeleteItem {
    fun deleteItem(position: Int)
}

class SourceListRecyclerViewAdapter(
    val list: MutableList<Origin>,
    private val listener: OnDeleteItem
) : RecyclerView.Adapter<SourceListRecyclerViewAdapter.ViewHolder>(), DeleteItem {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_source, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.url.text = item.url.toString()
    }

    override fun getItemCount(): Int = list.size

    override fun deleteItem(position: Int) {
        listener.onDeleteItem(list[position].url)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val url: TextView = view.findViewById(R.id.source_url)
    }


}
