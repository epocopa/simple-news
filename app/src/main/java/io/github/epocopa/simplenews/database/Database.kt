package io.github.epocopa.simplenews.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import io.github.epocopa.simplenews.Article
import io.github.epocopa.simplenews.Origin

@Database(entities = [ Origin::class, Article::class ], version = 1, exportSchema = false)
@TypeConverters(TypeConverter::class)
abstract class Database : RoomDatabase() {
    abstract fun articleDao(): ArticleDao
    abstract fun originDao(): OriginDao
}