package io.github.epocopa.simplenews

import android.app.Application

class SimpleNewsApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Repository.initialize(this)
    }
}
