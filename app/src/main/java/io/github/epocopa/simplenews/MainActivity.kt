package io.github.epocopa.simplenews

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.firebase.ui.auth.AuthUI
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import io.github.epocopa.simplenews.ArticleListFragment.Callbacks
import me.toptas.rssconverter.RssItem
import java.util.*

private const val TAG = "MainActivity"
private const val RC_SIGN_IN = 123
private const val ACTIVE_FRAGMENT_KEY = "activeFragment"

class MainActivity : AppCompatActivity(), Callbacks {
    private lateinit var auth: FirebaseAuth
    private val db = FirebaseFirestore.getInstance()
    private lateinit var bottomNavigationView: BottomNavigationView
    private val fragment1 = ArticleListFragment()
    private val fragment2 = SourceListFragment()
    private val fragment3 = SettingsFragment()
    private val favs = FavoriteListFragment()
    private var active: Fragment = fragment1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomNavigationView = findViewById(R.id.bottomNavigationView)
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()
        if (savedInstanceState != null) {
            active = supportFragmentManager.getFragment(savedInstanceState, ACTIVE_FRAGMENT_KEY)!!
        }
        addFragment(fragment3, "3")
        addFragment(fragment2, "2")
        addFragment(favs, "favs")
        addFragment(fragment1, "1")
    }

    public override fun onStart() {
        super.onStart()
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            return@setOnNavigationItemSelectedListener when (item.itemId) {
                R.id.page_1 -> {
                    supportFragmentManager.beginTransaction().hide(active).show(fragment1).commit()
                    active = fragment1
                    title = getString(R.string.app_name)
                    true
                }
                R.id.page_2 -> {
                    supportFragmentManager.beginTransaction().hide(active).show(fragment2).commit()
                    active = fragment2
                    title = getString(R.string.sources)
                    true
                }
                R.id.page_3 -> {
                    supportFragmentManager.beginTransaction().hide(active).show(fragment3).commit()
                    active = fragment3
                    title = getString(R.string.settings)
                    true
                }
                else -> false
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (auth.currentUser == null) {
            authenticate()
        } else {
            supportFragmentManager.beginTransaction().show(active).commit()
        }
    }

    override fun onFeedClick(article: RssItem) {
        Log.d(TAG, article.toString())
        CustomTabsIntent.Builder()
            .setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary))
            .build()
            .launchUrl(this, Uri.parse(article.link))
    }

    override fun onSignOut() {
        authenticate()
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        supportFragmentManager.putFragment(outState, ACTIVE_FRAGMENT_KEY, active)
    }

    override fun addFav(article: Article) {
        favs.addFav(article)
    }

    override fun onPause() {
        super.onPause()
        supportFragmentManager.beginTransaction().hide(active).commit()

    }

    private fun showHome() {
        supportFragmentManager.beginTransaction().hide(active).show(fragment1).commit()
        active = fragment1
        title = getString(R.string.app_name)
    }

    private fun authenticate() {
        startActivityForResult(
            // Get an instance of AuthUI based on the default app
            AuthUI
                .getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(
                    mutableListOf(
                        AuthUI.IdpConfig.EmailBuilder().build(),
                        AuthUI.IdpConfig.AnonymousBuilder().build()
                    )
                )
                .build(),
            RC_SIGN_IN
        )
    }

    override fun showFavs() {
        supportFragmentManager.beginTransaction().hide(active).show(favs).commit()
        active = favs
        title = getString(R.string.favorite)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            if (resultCode == Activity.RESULT_OK && !auth.currentUser?.isAnonymous!!) {
                val repository: Repository = Repository.get()
                val sourcesRef = db.collection("sources")
                    .document(auth.currentUser?.email!!)
                val favsRef = db.collection("favorites")
                    .document(auth.currentUser?.email!!)

                sourcesRef.get().addOnSuccessListener { document ->
                    val exists = hashMapOf(
                        "exists" to true
                    )
                    sourcesRef.set(exists, SetOptions.merge())

                    val sources = document["sources"] as List<String>?
                    if (sources != null) {
                        for (source in sources) {
                            repository.addOriginOffline(source)
                        }
                    }
                }.addOnFailureListener {
                    Log.d(TAG, it.message!!)
                }

                favsRef.get().addOnSuccessListener { document ->

                    val exists = hashMapOf(
                        "exists" to true
                    )
                    favsRef.set(exists, SetOptions.merge())
                    val favs = document["favorites"] as List<HashMap<String, String>>?
                    if (favs != null) {
                        for (fav in favs) {
                            val date = (fav["date"]!! as com.google.firebase.Timestamp).toDate()
                            val article = Article(fav["title"]!!, fav["origin"]!!, date)

                            repository.addArticleOffline(article)
                        }
                    }
                }.addOnFailureListener {
                    Log.d(TAG, it.message!!)
                }
            }
        }
    }


    private fun addFragment(fragment: Fragment, tag: String) {
        if (fragment !== active) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, fragment, tag)
                .hide(fragment)
                .commit()
        } else {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, fragment, tag)
                .commit()

        }
    }

    override fun onBackPressed() {
        if (active.tag == "favs") {
            showHome()
        } else {
            super.onBackPressed()

        }
    }
}


