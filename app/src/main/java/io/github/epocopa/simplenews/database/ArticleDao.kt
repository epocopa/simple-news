package io.github.epocopa.simplenews.database

import androidx.lifecycle.LiveData
import androidx.room.*
import io.github.epocopa.simplenews.Article

@Dao
interface ArticleDao {

    @Query("SELECT * FROM article")
    fun getArticles(): LiveData<List<Article>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addArticle(article: Article)

    @Update
    fun updateArticle(article: Article)

    @Delete
    fun deleteArticle(article: Article)

    // TODO REMOVE, for debugging
    @Query("DELETE FROM article")
    fun clearArticles()
}