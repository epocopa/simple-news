package io.github.epocopa.simplenews

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

class SourceListViewModel: ViewModel() {
    val repository: Repository = Repository.get()
    var sourcesLiveData: LiveData<List<Origin>>

    init {
        sourcesLiveData = repository.getOrigins()
    }
}