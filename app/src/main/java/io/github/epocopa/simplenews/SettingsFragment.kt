package io.github.epocopa.simplenews

import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat


class SettingsFragment : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.settings, rootKey)
        val themePreference: ListPreference? = findPreference(getString(R.string.theme_setting_key))
        AppCompatDelegate.setDefaultNightMode("${themePreference?.value}".toInt())
        themePreference?.onPreferenceChangeListener =
            Preference.OnPreferenceChangeListener { _, newValue ->
                AppCompatDelegate.setDefaultNightMode("$newValue".toInt())
                true
            }
    }
}