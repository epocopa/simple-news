package io.github.epocopa.simplenews.database

import androidx.room.TypeConverter
import java.net.URL
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class TypeConverter {
    private val formatter: DateFormat = SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy", Locale.US)

    @TypeConverter
    fun fromUrl(url: URL): String {
        return url.toString()
    }

    @TypeConverter
    fun toUrl(s: String): URL {
        return URL(s)
    }

    @TypeConverter
    fun fromDate(date: Date): String {
        return date.toString()
    }

    @TypeConverter
    fun toDate(s: String) : Date {
        return formatter.parse(s)
    }
}