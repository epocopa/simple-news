package io.github.epocopa.simplenews

import me.toptas.rssconverter.RssFeed
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

interface  RssService {
    @GET
    fun fetchRss(@Url url:String): Call<RssFeed>
}