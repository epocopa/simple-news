package io.github.epocopa.simplenews

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class AddSourceUITest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun addSource() {
        val source = "https://lorem-rss.herokuapp.com/feed?unit=second"
        try {
            val supportVectorDrawablesButton = onView(
                allOf(withId(R.id.anonymous_button), withText("Continue as guest"))
            )
            supportVectorDrawablesButton.perform(scrollTo(), click())
        } catch(e: NoMatchingViewException) {}

        val bottomNavigationItemView = onView(
            allOf(
                withId(R.id.page_2), withContentDescription("Organize Sources"),
                isDisplayed()
            )
        )
        bottomNavigationItemView.perform(click())

        val actionMenuItemView = onView(
            allOf(
                withId(R.id.add_source), withContentDescription("Add new source"),
                isDisplayed()
            )
        )
        actionMenuItemView.perform(click())

        val textInputEditText = onView(
            allOf(
                withId(R.id.input),
                isDisplayed()
            )
        )
        textInputEditText.perform(click())

        val textInputEditText2 = onView(
            allOf(
                withId(R.id.input),
                isDisplayed()
            )
        )
        textInputEditText2.perform(
            replaceText(source),
            closeSoftKeyboard()
        )

        val appCompatButton = onView(
            allOf(
                withId(android.R.id.button1), withText(android.R.string.ok)
            )
        )
        appCompatButton.perform(scrollTo(), click())

        val textView = onView(
            allOf(
                withId(R.id.source_url),
                withText(source),
                isDisplayed()
            )
        )
        textView.check(matches(withText(source)))
    }
}
